chrome.browserAction.onClicked.addListener(function(tab) {
	chrome.tabs.sendMessage(tab.id, {action: "click"}, null);
});

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
	switch(msg.action) {
		case "add_source":
			chrome.storage.local.get("sources", function(res) {
				var sources = res.sources;
				var item = {pattern: msg.pattern, name: msg.name, path: msg.path, host: msg.host};
				console.log("will add" + JSON.stringify(item));
				if(sources) {
					for(var i=0; i < sources.length; i++) {
						if(msg.host == sources[i].host && sources[i].name == msg.name) {
							console.log("source exisiting");
							sendResponse({result: false, msg: "There's already a source with this name on this page"});
							return;
						}
					}
					sources.push(item);
				} else {
					sources = [item]
				}

				chrome.storage.local.set({"sources": sources}, function(){
					if(!chrome.runtime.lastError) 
						console.log("Added", item);
					console.log("success");
					sendResponse({result: !chrome.runtime.lastError});
				});
			});
			break;
	}
});


function initWS() {
	var ws = new WebSocket("ws://localhost:13514/ws");

	ws.onopen = function() {
		console.log("Connection established");
	}

	ws.onclose = function() {
		setTimeout(initWS, 5000);
	}

	ws.onerror = function(error) {
		console.log("Websocket error:", error);
	}

	ws.onmessage = function(e) {
		var msg = JSON.parse(e.data);
		switch(msg.action) {
			case "get_domains":
				listDomains(function(result){
					var resp = {id: msg.id, action: "get_domains", data: result};
					console.log("Sending:", resp);
					ws.send(JSON.stringify(resp));
				});
				break;
			case "get_sources":
				listSources(msg.data.domain, function(result) {
					var resp = {id: msg.id, action: "get_sources", data: result};
					console.log("Sending:", resp);
					ws.send(JSON.stringify(resp));
				});
				break;
			case "read":
				readSource(msg.data.domain, msg.data.source, msg.data.attribute, function(result) {
					var resp = {id: msg.id, action: "read", data: result};
					console.log("Sending:", resp);
					ws.send(JSON.stringify(resp));
				});
				break;
			case "write":
				writeSource(msg.data.domain, msg.data.source, msg.data.attribute, msg.data.data);
				break;
			case "remove":
				removeSource(msg.data.domain, msg.data.source, function(success, errormsg) {
					var resp = {id: msg.id, action: "remove", data: {success: success, msg: errormsg}};
					console.log("Sending", resp);
					ws.send(JSON.stringify(resp));
				});
		}
	}
}



function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function listDomains(r) {
	chrome.storage.local.get("sources", function(resp) {
		if(chrome.runtime.lastError) {
			console.log(chrome.runtime.lastError);
			r([]);
			return
		}

		var sources = resp.sources || [];
		sources = sources.map(function(val) {
			return val.host;
		}).filter(onlyUnique);

		var availableDomains = []
		var promises = []
		sources.forEach(function(val, i){
			var deferred = $.Deferred();
			promises.push(deferred.promise());
			listSources(val, function(availableSources) {
				if(availableSources.length > 0) { 
					availableDomains.push(val);
				} 				

				deferred.resolve();
			});
		});

		$.when.apply($, promises).done(function(){
			console.log("all domains checked");
			r(availableDomains);
		});
	});
}

function listSources(domain, r) {
	chrome.storage.local.get("sources", function(resp) {
		if(chrome.runtime.lastError) {
			console.log(chrome.runtime.lastError);
			r([]);
			return;
		}

		var sources = resp.sources || [];
		sources = sources.filter(function(i){
			return i.host == domain;
		}).map(function(i){
			return i.name;
		});

		console.log("possible sources for", domain, sources);

		var availableSources = [];
		var promises = [];
		sources.forEach(function(val, i) {
			var deferred = $.Deferred();
			promises.push(deferred.promise());
			readSource(domain, val, "", function(content) {
				console.log("source", val, ":" + content);
				if(content != "Page not open") {
					console.log("source", val, "is available")
					availableSources.push(val)
				} else {
					console.log("source", val, "is not available")
				}
				deferred.resolve();
			});
		});

		$.when.apply($, promises).done(function(){
			console.log("every source checking done");
			r(availableSources);
		});
	});
}

function findSource(domain, sourceName, callback) {
	chrome.storage.local.get("sources", function(resp) {
		if(!resp){
			console.log("no response:", chrome.runtime.lastError);
			callback();
			return
		}

		if(!resp.sources) {
			console.log("no sources");
			callback();
			return;
		}

		var source = resp.sources.filter(function(v){
			return v.host == domain && v.name == sourceName;
		})[0];

		if(!source) {
			console.log("no such souce");
			callback();
			return
		}

		callback(source);
	});
}

function findTab(domain, sourceName, callback) {
	findSource(domain, sourceName, function(source) {
		if(!source){
			callback();
			return;
		}

		var query = {url: source.pattern};

		chrome.tabs.query(query, function(r) {
			var tab = r[0];
			if(!tab) {
				callback();
				return;
			}
			
			callback(tab, source);
		});	

	});
}

function readSource(domain, sourceName, attribute, callback) {
	findTab(domain, sourceName, function(tab, source) {	
		if(!tab) {
			callback("Page not open");
			return;
		}
		chrome.tabs.sendMessage(tab.id, {action: "read", path: source.path, attribute: attribute}, function(resp){
			if(resp && resp.success) {
				callback(resp.value);
			} else { 
				console.log("Got malformed response");
				callback("");
			}
		});
	});
}

function writeSource(domain, sourceName, attribute, data) {
	findTab(domain, sourceName, function(tab, source) {	
		if(!tab) {
			return;
		}
		chrome.tabs.sendMessage(tab.id, {action: "write", path: source.path, attribute: attribute, data: data}, null);
	});
}

function removeSource(domain, sourceName, callback) {
	chrome.storage.local.get("sources", function(resp) {
		if(!resp) {
			callback(false, runtime.lastError.message);
			return;
		}

		var sources = resp.sources;

		if(!sources) {
			callback(true);
		}
		
		sources = sources.filter(function(i) {
			return domain != i.host || sourceName != i.name;
		});

		chrome.storage.local.set({sources:sources}, function(){
			callback(!chrome.runtime.lastError, chrome.runtime.lastError?chrome.runtime.lastError.message:null);
		});
	});
}

initWS();

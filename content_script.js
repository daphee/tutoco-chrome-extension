var active = false;

$("body").append('<div id="tutoco_overlay" style="position: absolute; background-color: red; border: 2px solid rgb(139, 0, 0); opacity: 0.5; z-index: 10000; width:20px; height:20px; display: none;"></div>');

var overlay = $("#tutoco_overlay")[0];

chrome.runtime.onMessage.addListener(
	function(message, sender, sendResponse) {
		console.log("received message:", message);
		if(message.action == "click") {
			active = !active;
			console.log(active?"activating":"deactivating");
			if(!active) {
				overlay.style.display = "none";
			}
			register();
		} else if(message.action == "read") {
			var val = read(getElementByPath(message.path), message.attribute);
			console.log("read:'"+val+"'");
			sendResponse({success: true, value: val});
		} else if(message.action == "write") {
			write(getElementByPath(message.path), message.attribute, message.data);
		}
});

function read(el, attribute) {
	console.log("reading on", el);
	if(attribute) {
		return $(el).attr(attribute)
	}
	return $(el).text() || $(el).val() || $(el).html();
}

function write(el, attribute, data) {
	data = data.trim("\n");
	if(attribute) {
		$(el).attr(attribute, data);
	} else {
		if(data == "click") {
			$(el).click();
		} else {
			$(el).text(data).val(data).html(data);
		}
	}
}

function computePath(el) {
	function process(el) {
		console.log("processing:", el);
		var item = el.tagName;

		if(!el.tagName) {
			//we reached the document
			if(isFrame(el)) {
				console.log("is frame");
				var frame = getFrameForDocument(el);
				if(!frame) {
					window.alert("ERROR");
					return;
				}
				item = "$" + (frame.name || frame.id);
			} else {
				item = "$";
			}

			return item;
		}

		if(el.id) {
			item += "#" + el.id;
			return process(el.ownerDocument) + ":" + item;
		} else  {
			//add class
			var classes = el.getAttribute("class");
			if(classes) {
				classes = classes.split(" ");
				var lowestNum, lowestClass;
				for(var i=0; i < classes.length; i++) {
					var c = classes[i].trim();
					if(!c) continue;

					if(!lowestNum) {
						lowestNum = $(el.parentElement).children(item + "." + c).length;
						lowestClass = classes[i];
					} else if(lowestNum > $(el.parentElement).children(item + "." + c).length) {
						lowestNum = $(el.parentElement).children(item + "." + c).length;
						lowestClass = classes[i];
					}
				}
				if(lowestClass)
					item += "." + lowestClass;
			}

			console.log("Current item after class:" + item);

			if(el.parentElement) {
				if($(el.parentElement).find(item).length > 1) {
					console.log("Found more items matching '" + item +"'");
					$(el.parentElement).find(item).each(function(i, val) {
						if(val == el) {
							item += "[" + i + "]";
						}
					});
				}

				console.log("processing next:", el.parentElement);
				return process(el.parentElement) + "->" + item;
			} else {
				return process(el.ownerDocument) + ":" + item;
			}
		}
	}
	var path = process(el);
	console.log("whole path is:", path);
	return path;
}

function getDocumentByName(name) {
	for(var i=0; i < window.frames.length; i++) {
		try {
			if(window.frames[i].frameElement.id == name) {
				return window.frames[i].document;
			}
		} catch(err){}
	}
}

function getElementByPath(path) {
	var frame = path.slice(1, path.indexOf(":"))
	path = path.slice(path.indexOf(":") + 1);
	var el = document;
	if(frame) {
		el = getDocumentByName(frame);
	}

	function step() {
		if(path.indexOf("->") > 0) {
			var item = path.slice(0, path.indexOf("->"));
			path = path.slice(path.indexOf("->") + 2);
		} else {
			var item = path;
			path = "";
		}

		console.log("processing:"+item);
	
		if(item[item.length - 1] == "]") {
			var num = item.slice(item.lastIndexOf("[") + 1, item.length - 1);
			console.log("will fetch num:"+num);
			item = item.slice(0, item.lastIndexOf("["));
		}

		var result = $(el).find(item);
		if(result.length == 0) {
			console.log("error finding:"+item,"in",el);
			return;
		}
		if(num) {
			el = result[num];
		} else {
			el = result[0];
		}

		if(path)
			return step();
		else 
			return el;
	}

	return step();
}

//http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function addDataSource() {
	var el = lastElement;
	var path = computePath(el);
	var name = window.prompt("Please enter the new data source's name");
	if(!name) 
		return;
	var pattern = window.prompt("Url pattern:", window.location.protocol + "//" + window.location.host + window.location.pathname + "*");
	if(!pattern)
		return;
	chrome.runtime.sendMessage({action: "add_source", path: path, name:name, pattern: pattern, host: window.location.host}, function(resp) {
		console.log("got response:"+JSON.stringify(resp));
		if(resp.result) {
			window.alert("Looks good!");
		} else {
			if(resp.msg) {
				window.alert(resp.msg);
			} else {
				window.alert("Unknown error");
			}
		}
	});
}

var lastElement;
//
//Descriptive functions ;)
function isFrame(doc) {
	return doc.location.href != window.location.href;
}

function getFrameForDocument(doc) {
	return $("iframe[src|='"+doc.location.href+"']")[0];
}

function myElementFromPoint(x, y) {
	//we know that in handlers[] there are only frames we have access to
	var highest;
	for(var i=0; i < handlers.length; i++) {
		var element;
		if(isFrame(handlers[i].document)) {
			var frame = getFrameForDocument(handlers[i].document);
			if(!frame)
				continue;
			var rect = frame.getBoundingClientRect();
			element = handlers[i].document.elementFromPoint(x - rect.left, y - rect.top);
		} else {
			element = handlers[i].document.elementFromPoint(x, y);
		}
		if(element)
			if(!highest || highest.style.zIndex < element.style.zIndex) 
				highest = element;
	}
	return highest;
}

var x, y;

function drawOverlay() {
	//get the element
	var element = myElementFromPoint(x, y);

	//if the cursor is over the overlay
	if(element == overlay) {
		//check whats beneath
		overlay.style.display = "none";
		element = myElementFromPoint(x, y);
	}


	if(!element) {
		return;
	}

	if(lastElement == element) {
		//we checked whats beneath so diplay the overlay again
		overlay.style.display = "";
		return;
	}

	overlay.style.display = "";
	var rect = element.getBoundingClientRect();
	if(isFrame(element.ownerDocument)) {
		var frame = getFrameForDocument(element.ownerDocument);
		if(!frame) {
			console.log("Couldn't get frame for frame:"+element.ownerDocument.location.href);
			return;
		}
		var rect2 = frame.getBoundingClientRect();
		$(overlay).css("top", window.scrollY + rect.top + rect2.top).css("left", window.scrollX + rect.left + rect2.left).css("width", rect.width).css("height", rect.height);
	} else {
		$(overlay).css("top", window.scrollY + rect.top).css("left", window.scrollX + rect.left).css("width", rect.width).css("height", rect.height);
	}
	lastElement = element;

}

function mouse(doc) {
	return function(e) {
	 	x = e.x;
		y = e.y;

		if(doc != document) {
			//heavily optimized to work in spotify. very likely to not work everywhere
			var frame = getFrameForDocument(doc);
			if(!frame) {
				return;
			}
			var rect = frame.getBoundingClientRect();
			x += rect.left;
			y += rect.top
		}
		
		if(!active) {
			return;
		}

		drawOverlay();

	}
}


var handlers = [];

function register() {
	for(var i=0; i < handlers.length; i++) {
		handlers[i].document.removeEventListener("mousemove", handlers[i].handler, true);
	}

	handlers = [];

	for(var i=0; i < window.frames.length; i++) {
		try {
			var handler = mouse(window.frames[i].document);
			window.frames[i].document.addEventListener("mousemove", handler, true);
			handlers.push({document: window.frames[i].document, handler: handler});
		} catch(err){}
	}

	var handler = mouse(document);
	document.addEventListener("mousemove", handler, true);
	handlers.push({document: document, handler: handler});
}

overlay.onclick = function(e) {
	addDataSource();
	active = false;
	overlay.style.display = "none";
}

var framesToLoad = 0;
